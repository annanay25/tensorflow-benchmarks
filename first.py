from __future__ import print_function

import tensorflow as tf
import numpy
import time 
import os

# os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# Import MNIST data
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("/tmp/tensorflow/mnist/input_data", one_hot=True)

# Parameters
learning_rate = 0.001
training_iters = 2000
batch_size = 128
display_step = 10

# Network Parameters
n_input = 784 # MNIST data input (img shape: 28*28)
n_classes = 10 # MNIST total classes (0-9 digits)
dropout = 0.75 # Dropout, probability to keep units

# tf Graph input
x = tf.placeholder(tf.float32, [None, n_input])
y = tf.placeholder(tf.float32, [None, n_classes])
keep_prob = tf.placeholder(tf.float32) #dropout (keep probability)

# Create some wrappers for simplicity
def conv2d(x, W, b, strides=1):
    x = tf.reshape(x, shape=[-1, 28, 28, 1])
    # Conv2D wrapper, with bias and relu activation
    x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding='SAME')
    x = tf.nn.bias_add(x, b)
    return x

# Store layers weight & bias
weights = {
    # 5x5 conv, 1 input, 32 outputs
    'wc1': tf.Variable(tf.random_normal([5, 5, 1, 32])),
}

biases = {
    'bc1': tf.Variable(tf.random_normal([32])),
}

# Initializing the variables
init = tf.global_variables_initializer()

# Launch the graph
config = tf.ConfigProto(
    intra_op_parallelism_threads=1,
    inter_op_parallelism_threads=1
)
jit_level = tf.OptimizerOptions.ON_1
config.graph_options.optimizer_options.global_jit_level = jit_level
run_metadata = tf.RunMetadata()
with tf.Session(config=config) as sess:
    sess.run(init)
    # Keep training until reach max iterations
    # while step * batch_size < training_iters:
    x = mnist.train.next_batch(batch_size)
    beg = time.time()
    sess.run(conv2d(x[0][0], weights['wc1'], biases['bc1']))
    end = time.time()
    print(end - beg)
    print (numpy.shape(x[0][0]))

